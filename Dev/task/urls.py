from django.contrib import admin
from django.urls import path
from task import views
from django.conf.urls import url
from django.contrib.auth import views as auth_views



urlpatterns = [
    url(r'^account', views.account, name='account'),
    url(r'^login/', views.login_view, name='login'),
    url(r'^registration/$', views.registration, name='registration'),
    url(r'^change_password/$', views.change_password, name='change_password'),
    url(r'^login_page/', views.login_page, name='login_page'),
    path('admin/', admin.site.urls),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
]

