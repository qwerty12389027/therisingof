# _*_ coding: UTF-8 _*_

from django.db import models


class Accounts(models.Model):
    id_user = models.AutoField(primary_key=True)
    name = models.CharField(max_length=16, default="Null")
    password = models.CharField(max_length=16, default="Null")
    email = models.CharField(max_length=64, default="Null")


class Deck(models.Model):
    id_deck = models.ForeignKey(Accounts, to_field='id_user', null=False, on_delete=models.CASCADE)
    id_card = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=16, default="Null")


class Card(models.Model):
    id_card = models.ForeignKey(Deck, null=False, on_delete=models.CASCADE)
    name = models.CharField(max_length=16, default="Null")
    img = models.ImageField(upload_to="", default="Null")
    stat = models.IntegerField(default="0")



