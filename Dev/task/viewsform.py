from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from .forms import LoginForm, UserRegistrationForm



# Эта форма будет использована для аутентификации пользователей через базу данных,
# шаблон URL-адреса для этого представления еще не создан
def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(username=cd['username'], password=cd['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponse('Authenticated successfully')
                else:
                    return HttpResponse('Disabled account')
            else:
                return HttpResponse('Invalid login')
    else:
        form = LoginForm()
    return render(request, 'account/login.html', {'form': form})


# Представление для создания учетных записей пользователей
# Вместо сохранения необработанного пароля, введенного пользователем,
# используем метод set_password() модели User, обрабатывающей шифрование для сохранения безопасности.
# Далее необходимо отредактировать файл urls.py приложения task и вставить url-шаблон:
#  url(r'^register/$', views.register, name='register'),

def registration(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():#Если форма заполнена правильно
            form.save()#Создаем пользователя
            username = form.cleaned_data.get('username')#с именем
            raw_password = form.cleaned_data.get('password1')# и паролем
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('index')#страница редиректа после регистрации
    else:
        form = UserRegistrationForm()
    return render(request, 'signup.html', {'form': form})