from django.http import *
from django.template import loader
from django.contrib import messages
from django.shortcuts import render, redirect
from django.contrib.auth.forms import PasswordChangeForm, UserCreationForm 
from django.contrib.auth import  login, logout, authenticate, get_user_model, update_session_auth_hash
from django.contrib.auth.models import User




def login_view(request):
    if request.method == 'POST':
        username = request.GET['username']
        password = request.GET['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            messages.success("Авторизация прошла успешно")
            return HttpResponseRedirect("/login_page/")
        else:
             messages.error("Введены неверные данные")
             return HttpResponseRedirect("/login_page/")



def login_page(request):
    template = loader.get_template("login.html")
    return HttpResponse(template.render())


def logout_view(request):
    logout(request)
    return HttpResponseRedirect("/login_page/")


def registration(request):
   if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('login_page')
    else:
        form = UserCreationForm()
        return render(request, 'registration.html', {'form': form})



def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            messages.success(request, 'Your password was successfully updated!')
            return redirect('change_password')
        else:
            messages.error(request, ' The error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'change_password.html', {'form': form})


