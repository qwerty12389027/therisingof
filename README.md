Проект "Коллекционная карточная игра"

Участники: Бреннер Вадим, РИ-380002

[Trello](https://trello.com/invite/b/a6xLZYmS/10e3a8c0798961e34efd2b44dc7c08ab/%D0%BA%D0%BA%D0%B8)

 tests.py, 
 views.py    находятся в Dev/task

    
Last update   28.12.2020:

   Добавлены:

1) templates

2) venv, site-packages позже отправятся в папку Lib

   Исправления в:

1) .gitlab-ci.yml

2) settings.py

3) urls.py

4) views.py

5) tests.py
