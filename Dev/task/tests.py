from django.test import TestCase
from task.models import Accounts
from django.contrib.auth import get_user_model


class AccountsTestCase(TestCase):
    def setUp(self):
        Accounts.objects.create(id_user=0, name="lion", password="roar")
        Accounts.objects.create(id_user=1, name="cat", password="meow")

    def test_animals(self):
        lion = Accounts.objects.get(name="lion")
        cat = Accounts.objects.get(name="cat")
        self.assertEqual(lion.password, 'roar')
        self.assertEqual(cat.password, 'meow')
        

class LoginTestCase(TestCase):
    def setUp(self):
        user = User.create_user(username='user1', password='12345')
        admin = User.objects.create_user(username='admin', password='adminpd', is_superuser=1, is_staff=1)


    def test_login_user(self):
        self.assertEqual(self.client.get('/login/', {'username': 'user1', 'password': '12345'}).url, '/')
        self.assertEqual(self.client.get('/login/', {'username': 'user1', 'password': '54321'}).url, '/login_page/')

        self.assertEqual(self.client.login(username='user1', password='12345')
        self.assertEqual(self.client.login(username='user1', password='54321')



    def test_admin(self):
        self.client.get('/login/', {'username': 'admin', 'password': 'adminpd'})
        response = self.client.get('/admin/')
        self.assertEqual(response.status_code, 200)




class LogoutTestCase(TestCase):
    def setUp(self):
        User = get_user_model()
        admin = User.objects.create_user(username='admin',password='adminpd', is_superuser=1, is_staff=1)


    def test_logout(self):
        self.client.get('/login/', {'username': 'admin', 'password': 'adminpd'})
        response = self.client.get('/admin/')
        self.assertEquals(response.status_code, 200)
        self.client.get('/logout/')
        response = self.client.get('/admin/')
        self.assertEquals(response.status_code, 302)


class CheckChangePassword(TestCase):
    def setUp(self):
        user4 = User.objects.create_user(username='user4', password='user44')
        admin = User.objects.create_user(username='admin', password='old_admin', is_superuser=1, is_staff=1)


    def test_new_password_user_two(self):
        self.client.get('/login/', {'username': 'user4', 'password': 'user44'})
        self.client.post('/change_password/', {'old_password': 'test_user','new_password': 'user55'})
        self.client.get('/logout/')

        self.assertEqual(self.client.get('/login/', {'username': 'user4', 'password': 'user44'}).url, '/login_page/')
        self.assertEqual(self.client.get('/login/', {'username': 'user4', 'password': 'user55'}).url, '/login_page/')


    def test_new_password_admin(self):
        self.client.get('/login/', {'username': 'admin', 'password': 'old_admin'})
        self.client.post('/change_password/', {'old_password': 'old_admin','new_password': 'new_admin_psd'})
        self.client.get('/logout/')
        
        self.assertEqual(self.client.get('/login/', {'username': 'admin', 'password': 'old_admin'}).url, '/login_page/')
        self.assertEqual(self.client.get('/login/', {'username': 'admin', 'password': 'new_admin_psd'}).url, '/login_page/')
        self.client.get('/logout/')

        response = self.client.get('/admin/')
        self.assertEqual(response.status_code, 302)
        self.client.get('/login/', {'username': 'admin', 'password': 'new_admin_psd'})
        response = self.client.get('/admin/')
        self.assertEqual(response.status_code, 200)
